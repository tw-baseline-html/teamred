## Pipeline 提交守则

* 每一个时刻只能够有一个 pipeline 实例运行。在 pipeline 正在执行代码检查和构建的过程中不得 push 新代码。
* 如果出现 pipeline 的某一个 stage 失败。则其他人全部停止提交代码。大家要做的惟一的事情就是将 pipeline 修好。
* Pipeline 禁止红着过夜。每天晚上 11:00 之后禁止提交代码。
* 每一个提交的 git commit 的格式请使用 semantic commit message。具体请参见附录。

## 附录

### Semantic Commit Message

```
feat: add hat wobble
^--^  ^------------^
|     |
|     +-> Summary in present tense.
|
+-------> Type: chore, docs, feat, fix, refactor, style, or test.
```


* feat: (new feature for the user, not a new feature for build script)
* fix: (bug fix for the user, not a fix to a build script)
* docs: (changes to the documentation)
* style: (formatting, missing semi colons, etc; no production code change)
* refactor: (refactoring production code, eg. renaming a variable)
* test: (adding missing tests, refactoring tests; no production code change)
* chore: (updating grunt tasks etc; no production code change)
